<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Profile
 *
 * @author dsaez
 */
class Profile {
    
    private $idProfile;
    private $name;
    private $avatar;
    private $description;
    
    public function __construct(){
        
    }
    
    public function getIdProfile(){
        return $this->idProfile;
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function getAvatar() {
        return $this->avatar;
    }
    
    public function getDescription() {
        return $this->description;
    }
    
    public function setIdProfile($idProfile) {
        $this->idProfile = $idProfile;
    }

    public function setName($name) {
        $this->name = $name;
    }
    
    public function setAvatar($avatar) {
        $this->avatar = $avatar;
    }

    function setDescription($description) {
        $this->description = $description;
    }
    
    //Función para pintar cada criatura
    function profile2HTML() {
        $result = '<div class=" col-md-4 ">';
            $result .= '<div class="card ">';
                $result .= ' <img class="card-img-top rounded mx-auto d-block avatar" src='.$this->getAvatar().' alt="Card image cap">';
                $result .= '<div class="card-block">';
                    $result .= '<h2 class="card-title">' . $this->getName() . '</h2>';
                    $result .= '<p class=" card-text description">'.$this->getDescription().'</p>';                    
                $result .= '</div>';
                $result .= ' <div  class=" btn-group card-footer" role="group">';
                    $result .= '<a type="button" class="btn btn-secondary" href="app/views/detail.php?id='.$this->getIdProfile().'">Det</a>';
                    $result .= '<a type="button" class="btn btn-success" href="app/views/edit.php?id='.$this->getIdProfile().'">Mod</a> ';
                    $result .= '<a type="button" class="btn btn-danger" href="app/controllers/deleteController.php?id='.$this->getIdProfile().'">Del</a> ';
                $result .= ' </div>';
            $result .= '</div>';
        $result .= '</div>';
        
        return $result;
    }
}
