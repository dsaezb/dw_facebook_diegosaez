<?php
/**
 * @title: Proyecto integrador Ev01 - Registro en el sistema.
 * @description:  Script PHP para almacenar un nuevo usuario en la base de datos
 *
 * @version    0.1
 *
 * @author ander_frago@cuatrovientos.org
 */

require_once '../templates/header_app.php';
//MADE completa los requiere que necesites
require_once '../persistence/conf/PersistentManager.php';
require_once '../persistence/DAO/UserDAO.php';
require_once '../utils/SessionHelper.php';

$error = $user = $pass = "";

if (isset($_POST['email'])) {

  // MADE Realiza la lectura de los campos del formulario en $user y $pass
  $user = $_POST['email'];
  $pass = $_POST['password'];

  if ($user == "" || $pass == "") {
    $error = "Debes completar todos los campos<br><br>";
  }
  else {
      // TODO Error en la lectura de uDAO
      $uDAO = new UserDAO();
      if ($uDAO->checkExistsUsers($user)){
          $error = "<span class='error'>Este usuario ya esta registrado</span><br><br>";
      } else{
       // MADE Obten una instancia de PersistenManager
     $pm = PersistentManager::getInstance();
     // MADE Inserta el nuevo usuario atraves de un objeto UserDAO
     $uDAO->insert($user, $pass);


     // MADE establece el usuario en sesión con SessionHelper
     SessionHelper::setSession($user);
     header('Location: login.php?');   
      }
  }
}
?>
<div>
    <form action="signup.php" method="post">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h2>Registrate para obtener tus privilegios de admin</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="form-group has-danger">
                    <label class="sr-only" for="email">Email:</label>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"><i
                                    class="fa fa-at"></i></div>
                        <input type="text" name="email" class="form-control"
                               id="email"
                               placeholder="vivayo@correo.com" required
                               autofocus>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-control-feedback">
                    <span class="text-danger align-middle">
                        <i class="fa fa-close"></i><?php  //MADE Muestra el mensaje de error  
                           if(isset($error)){
                               echo $error;
                           }
                          ?>
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="sr-only" for="password">Contraseña:</label>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"><i
                                    class="fa fa-key"></i></div>
                        <input type="password" name="password"
                               class="form-control" id="password"
                               placeholder="Password" required>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-control-feedback">
                    <span class="text-danger align-middle">
                         <?php  //MADE Muestra el mensaje de error  
                           if(isset($error)){
                               echo $error;
                           }
                          ?>
                    </span>
                </div>
            </div>
        </div>

        <div class="row" style="padding-top: 1rem">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-success"><i
                            class="fa fa-sign-in"></i> Acceder
                </button>
            </div>
        </div>
    </form>
</div>