<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/ProfileDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Profile.php');
require_once(dirname(__FILE__) . '/../../app/models/validations/ValidationsRules.php');



if ($_SERVER["REQUEST_METHOD"] == "POST") {
//Llamo a la función en cuanto se redirija el action a esta página
    createAction();
}

function createAction() {
    $name = ValidationsRules::test_input($_POST["name"]);
    $description = ValidationsRules::test_input($_POST["description"]);
    $avatar = ValidationsRules::test_input($_POST["avatar"]);
    // TODOD hacer uso de los valores validados 
    $profile = new Profile();
    $profile->setName($_POST["name"]);
    $profile->setDescription($_POST["description"]);
    $profile->setAvatar($_POST["avatar"]);

    
    $profileDAO = new ProfileDAO();
    $profileDAO->insert($profile);
    
    header('Location: ../../index.php');
    
}
?>

