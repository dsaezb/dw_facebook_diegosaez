<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/ProfileDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Profile.php');


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    editAction();
}

function editAction() {
    
    $id = $_POST["id"];
    $name = $_POST["name"];
    $avatar = $_POST["avatar"];
    $description = $_POST["description"];

    $profile = new Profile();
    $profile->setIdProfile($id);
    $profile->setName($name);
    $profile->setAvatar($avatar);
    $profile->setDescription($description);

    $profileDAO = new ProfileDAO();
    $profileDAO->update($profile);

    header('Location: ../../index.php');
}

?>

