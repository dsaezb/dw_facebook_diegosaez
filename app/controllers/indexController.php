<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/ProfileDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Profile.php');


function indexAction() {
    $profileDAO = new ProfileDAO();
    return $profileDAO->selectAll();
}

?>