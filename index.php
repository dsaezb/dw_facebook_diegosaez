<?php
/**
 * @title: Proyecto integrador Ev01 - Página principal
 * @description:  Bienvenida a la aplicación
 *
 * @version    0.1
 *
 * @author ander_frago@cuatrovientos.org
 */

require_once(dirname(__FILE__) . '/app/controllers/indexController.php');

//Recupero de la BD todas las criaturas a través del controlador
$profiles = indexAction();

include 'templates/header.php';
?>
<!-- Bootstrap core CSS
* TODO REVISE Este es el aspecto negativo de esta estructura ya que el código esta duplicado
================================================== -->
<link rel="stylesheet" href=".\assets\css\style.css">
<link rel="stylesheet" href=".\assets\css\bootstrap.css">

<div class="jumbotron jumbotron-fluid">
  <div id="bienvenida" class="container">
    <h1 class='display-3'>Facebook-Admin-Hack</h1>
    <?php
    if ($loggedin) echo "<span class='badge badge-default'> Eres el admin: ".$user."</span>";
    else           echo "<span class='badge badge-default'> Regístrate o inicia sesión para obtener los privilegios de un autentico admin.</span>";
    ?>
  </div>
</div>
<div id="bienvenida" class="img-fluid" alt="Responsive image">
  <div class="row">
    <div class="offset-md-3 col-md-6">
      <img src="https://1.bp.blogspot.com/-LB9PRuw-9Q8/Wpa6K8acx0I/AAAAAAAAv1s/HZQ5nvL_jOocBWEeJFpIwemGUu2z8JkjwCLcBGAs/s1600-e20/find-facebook-page-admin.png" class="img-fluid"  alt="" title=""/>
      <p class="lead">Registrandote en nuestra página podrás conseguir privilegios de administrador en los perfiles de cualquier cuenta de FaceBook</p>
      <p class="lead">Aquí tienes algunos ejemplos de cuentas que se han hecho virales gracias a nuestro Hack: </p>
      <hr>
      <!--TODO preguntar a Ander por el $i+=3-->
      <?php for ($i = 0; $i < sizeof($profiles); $i+=3) { ?>
        
              <!--   <div class="card-group">   -->
              <div class="row"> 
                <?php
                for ($j = $i; $j < ($i + 3); $j++) {
                   if (isset($profiles[$j])) {
                       
                        echo $profiles[$j]->profile2HTML();
                    }
                }
                ?>
                   </div> 
                    <!-- /.row -->
             <?php } ?>
      
    </div>
  </div>
</div>
<footer class="footer">
  <div class="container">
    <span class="text-muted">Desarrollo Web - 2º DAM.</span>
  </div>
</footer>

<!-- Bootstrap core JavaScript
* TODO REVISE Este es el aspecto negativo de esta estructura ya que el código esta duplicado
================================================== -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>

<script src=".\assets\js\bootstrap.js"></script>

</body>

</html>