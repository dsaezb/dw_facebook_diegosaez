CREATE DATABASE IF NOT EXISTS facebookdb;

USE facebookdb;

CREATE TABLE IF NOT EXISTS profiles (
	idProfile int not null AUTO_INCREMENT,
	name varchar(255),
	description text,
	avatar varchar(255),
	PRIMARY KEY( idProfile )
);


INSERT INTO `profiles` VALUES (1,'El Pepe',' Meme viral de 2020 originario de Republica dominicana.','https://i.ytimg.com/vi/DwlzqcGhITA/hqdefault.jpg');

INSERT INTO `profiles` VALUES (2,'El Sech',' Meme viral de 2019 originario de Panamá.','https://i.ytimg.com/vi/vGJ1ZdPIArk/hqdefault.jpg');

INSERT INTO `profiles` VALUES (3,'El chavo',' Meme viral de 1990 originario de México.','https://www.elnuevoheraldo.com/wp-content/uploads/sites/66/2020/09/Donde-estan-los-vecinos-de-E_1198257.jpg');