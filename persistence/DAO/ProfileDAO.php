<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProfileDAO
 *
 * @author dsaez
 */

require_once(dirname(__FILE__) . '/../conf/PersistentManager.php');

class ProfileDAO {
    
    //Se define una constante con el nombre de la tabla
    const PROFILES_TABLE = 'profiles';

    //Conexión a BD
    private $conn = null;

    //Constructor de la clase
    public function __construct() {
        $this->conn = PersistentManager::getInstance()->get_connection();
    }

    public function selectAll() {
        $query = "SELECT * FROM " . ProfileDAO::PROFILES_TABLE;
        $result = mysqli_query($this->conn, $query);
        $profiles = array();
        while ($facebookDB = mysqli_fetch_array($result)) {

            $profile = new Profile();
            $profile->setIdProfile($facebookDB["idProfile"]);
            $profile->setName($facebookDB["name"]);
            $profile->setDescription($facebookDB["description"]);
            $profile->setAvatar($facebookDB["avatar"]);
            
            array_push($profiles, $profile);
        }
        return $profiles;
    }

    public function insert($profile) {
        $query = "INSERT INTO " . ProfileDAO::PROFILES_TABLE .
                " (name, description, avatar) VALUES(?,?,?)";
        $stmt = mysqli_prepare($this->conn, $query);
        $name = $profile->getName();
        $description = $profile->getDescription();
        $avatar = $profile->getAvatar();
        
        mysqli_stmt_bind_param($stmt, 'sss', $name, $description, $avatar);
        return $stmt->execute();
    }

    public function selectById($id) {
        $query = "SELECT name, description, avatar FROM " . ProfileDAO::PROFILES_TABLE . " WHERE idProfile=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $name, $description, $avatar);

        $profile = new Profile();
        while (mysqli_stmt_fetch($stmt)) {
            $profile->setIdProfile($id);
            $profile->setName($name);
            $profile->setDescription($description);
            $profile->setAvatar($avatar);
       }

        return $profile;
    }

    public function update($profile) {
        $query = "UPDATE " . ProfileDAO::PROFILES_TABLE .
                " SET name=?, description=?, avatar=?"
                . " WHERE idProfile=?";
        $stmt = mysqli_prepare($this->conn, $query);
        $name = $profile->getName();
        $description= $profile->getDescription();
        $avatar = $profile->getAvatar();
        $id = $profile->getIdProfile();
        mysqli_stmt_bind_param($stmt, 'sssi', $name, $description, $avatar, $id);
        return $stmt->execute();
    }
    
    public function delete($id) {
        $query = "DELETE FROM " . ProfileDAO::PROFILES_TABLE . " WHERE idProfile=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        return $stmt->execute();
    }

        
}

?>
